#!/bin/bas

#requirments  yum install mysql_utilities 
#remi repo should be included

#requirments: new db with name %db%_tmp, we will keep actiall state there

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
FILEDATE=$DIR"/ignore/scheme-"`eval date +%Y%m%d%H%M`".sql"
EXECUTED=$DIR"/ignore/executed.txt"
FILETMP=$DIR"/ignore/scheme-tmp.sql"
IGNOREDIR=$DIR"/ignore/"
ALTERNAME="alter-"`eval date +%Y%m%d%H%M`".sql"
CREATENAME="create-"`eval date +%Y%m%d%H%M`".sql"
DROPNAME="drop-"`eval date +%Y%m%d%H%M`".sql"
DATABASENAME="db-"`eval date +%Y%m%d%H%M`".sql"
ALTERDIR=$DIR"/alters/"
ALTERDATE=$DIR"/alters/"$ALTERNAME
DROPDATE=$DIR"/alters/drop-"`eval date +%Y%m%d%H%M`".sql"
FILE=$DIR"/scheme.sql"
FILESIZE=$(stat -c%s "$FILE")
FULL=$DIR"/fullbackup.sql"
T=$DIR'/ignore/tmp.txt'
I=$DIR'/ignore/tmpi.txt'
ID=$DIR'/ignore/tmpid.txt'
echo '' > $T
echo '' > $I
echo '' > $ID

## GET ACCESS TO DBS
if [ -n "$1" ]
then
	DB=$1
	DBTMP=$1"_tmp"
else
	echo "NO DB SELECTED"
	exit 0
fi

if [ -n "$2" ]
then
	USER=$2
else
	USER=root
fi

if [ -n "$3" ]
then
	PASSWORD=$3
else
	PASSWORD='123456'
fi

if [ -n "$4" ]
then
	LOCALIP=$4
else
	LOCALIP=`eval ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'`
fi

## INSTALATION 
if [[ ! -d $DIR"/ignore" ]]; then
    mkdir $DIR"/ignore"
    #git check
    if [[ -f ../.gitignore ]]
    then
    	echo '/db/ignore' >> ../.gitignore
    fi
    #executed list
    touch $EXECUTED
else
	#drop scheme older then 30 days
	find $IGNOREDIR  -maxdepth 1 -type f -mtime +30 -name "scheme*" -delete 
fi
if [[ ! -d $DIR"/alters" ]]; then
    mkdir $DIR"/alters"
fi

#mysql notification disable
mysql -h $LOCALIP -u $USER -p$PASSWORD -e "SET sql_notes = 0;"


# ## MYSQL CONFIGURATION DB
if [[ -z "`mysql -h $LOCALIP -u $USER -p$PASSWORD -qfsBe "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='$DB'" 2>&1`" ]];
then

	#we need create db
	mysql -h $LOCALIP -u $USER -p$PASSWORD -e "CREATE DATABASE $DB;"
	
	if [[ ! -z "`mysql -h $LOCALIP -u $USER -p$PASSWORD -qfsBe "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='$DB'" 2>&1`" ]];
	then
		echo "DB WAS CREATED: $DB"
		#now we need fill it with dump file
		#echo $FILESIZE
		if [ $FILESIZE -gt 5 ]
		then
			
			mysql -h $LOCALIP -u $USER -p$PASSWORD $DB < $FILE
			echo "EXCUTED MAIN DUMP"
			#and fill strict data
			for i in $( ls $ALTERDIR | grep "strict-" ); do
				if !( grep -q $i $EXECUTED )
				then
					echo $i >> $EXECUTED
					mysql -h $LOCALIP -u $USER -p$PASSWORD $DB < alters/$i
					echo "EXECUTED STRCT DUMP: $i"
				fi
		    done
		    #and escape previous alters, because they already in main dump
		    for i in $( ls $ALTERDIR | grep -e "alter-" | grep -e "create-" | grep -e "db-" | grep -e "drop-" ); do
				if !( grep -q $i $EXECUTED )
				then
					echo $i >> $EXECUTED
				fi
			done
		fi
		#drop tmp db to make sure they have identical structure
		mysql -h $LOCALIP -u $USER -p$PASSWORD -e "DROP DATABASE $DBTMP;"
		echo "TMP DROP DB: $DBTMP"
	else
		echo "SCRIPT CAN'T CREATE DB, RUN QUERY: CREATE DATABASE $DB;"
		exit 0
	fi
fi

if [[ -z "`mysql -h $LOCALIP -u $USER -p$PASSWORD -qfsBe "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='$DBTMP'" 2>&1`" ]];
then
	#we need create db tmp
	mysql -h $LOCALIP -u $USER -p$PASSWORD -e "CREATE DATABASE $DBTMP;"
	if [[ ! -z "`mysql -h $LOCALIP -u $USER -p$PASSWORD -qfsBe "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='$DBTMP'" 2>&1`" ]];
	then
		echo "TMP DB WAS CREATED: $DBTMP"
		#now we need fill it with dump file
		if [ $FILESIZE -gt 5 ]
		then
			mysql -h $LOCALIP -u $USER -p$PASSWORD $DBTMP < $FILE
			echo "TMP EXCUTED MAIN DUMP"
		fi
	else
		echo "SCRIPT CAN'T CREATE TMP DB, RUN QUERY: CREATE DATABASE $DBTMP;"
		exit 0
	fi
fi




if [ "$?" -eq 0 ]; then
	#will execute all alters before
	for i in $( ls $ALTERDIR | grep "drop-" ); do
		if !( grep -q $i $EXECUTED )
		then
			echo $i >> $EXECUTED
			mysql -h $LOCALIP -u $USER -p$PASSWORD $DB < $ALTERDIR$i
			mysql -h $LOCALIP -u $USER -p$PASSWORD $DBTMP < $ALTERDIR$i
			echo "DROP EXCUTED: $i"
		fi
    done

	for i in $( ls $ALTERDIR | grep "create-" ); do
		if !( grep -q $i $EXECUTED )
		then
			echo $i >> $EXECUTED
			mysql -h $LOCALIP -u $USER -p$PASSWORD $DB < $ALTERDIR$i
			mysql -h $LOCALIP -u $USER -p$PASSWORD $DBTMP < $ALTERDIR$i
			echo "CREATE EXCUTED: $i"
		fi
    done

	for i in $( ls $ALTERDIR | grep "db-" ); do
		if !( grep -q $i $EXECUTED )
		then
			echo $i >> $EXECUTED
			mysql -h $LOCALIP -u $USER -p$PASSWORD $DB < $ALTERDIR$i
			mysql -h $LOCALIP -u $USER -p$PASSWORD $DBTMP < $ALTERDIR$i
			echo "DB EXCUTED: $i"
		fi
    done

	for i in $( ls $ALTERDIR | grep "alter-" ); do
		if !( grep -q $i $EXECUTED )
		then
			echo $i >> $EXECUTED
			mysql -h $LOCALIP -u $USER -p$PASSWORD $DB < $ALTERDIR$i
			mysql -h $LOCALIP -u $USER -p$PASSWORD $DBTMP < $ALTERDIR$i
			echo "ALTER EXCUTED: $i"
		fi
    done

    #get dump
    mysqldump  -d -h $LOCALIP -u $USER -p$PASSWORD $DB --compact | sed 's/ AUTO_INCREMENT=[0-9]*\b//' > $FILETMP

    sed -i ':a;N;$!ba;s/\n/ /g' $FILETMP
	sed -i 's/;/;\n /g' $FILETMP
	sed -i 's/;/;\n/g' $FILETMP

    #get new table statements
    for table in $( mysqldiff --server1=$USER:$PASSWORD@$LOCALIP --server2=$USER:$PASSWORD@$LOCALIP --difftype=sql --changes-for=server2 --force --skip-table-options $DB:$DBTMP | grep "TABLE:" | sed 's/.*TABLE: //i' ); do
    	CREATESTATEMENT=`cat $FILETMP | grep "CREATE TABLE .$table. "`
    	if [ ${#CREATESTATEMENT} -gt 5 ]
    	then
    		echo "NEW TABLE ADDED: $table"
    		#cat $FILETMP | grep "CREATE TABLE .$table. " | sed 's/CREATE TABLE/CREATE TABLE IF NOT EXISTS/i'
    		cat $FILETMP | grep "CREATE TABLE .$table. " | sed 's/CREATE TABLE/CREATE TABLE IF NOT EXISTS/i' >> $ALTERDIR$CREATENAME
    	else
    		echo "TABLE DROPED: $table"
    		echo "DROP TABLE $table;" >> $ALTERDIR$DROPNAME
    	fi
    done

	#save difference for svn
	mysqldiff --server1=$USER:$PASSWORD@$LOCALIP --server2=$USER:$PASSWORD@$LOCALIP --difftype=sql --changes-for=server2 --force --skip-table-options $DB:$DBTMP | grep -v "#" | grep -v '\-\-\-'  | grep -v "/\*"| grep -v "+" | grep -v "@"  | grep -v "^$" | grep -v "Compare" | grep -v "Success" > $FILETMP
	#echo "mysqldiff --server1=$USER:$PASSWORD@$LOCALIP --server2=$USER:$PASSWORD@$LOCALIP --difftype=sql --changes-for=server2 --force --skip-table-options $DB:$DBTMP"

	#cat $FILETMP
	#here we will split alters
	# echo "in---"
	# cat $FILETMP

	while read p; 
	do
		if [[ $p =~ 'DROP PRIMARY KEY'.* ]];
		then
			DPK=true
			pc=`echo $p | sed 's\,$\;\i'`
			DPKCMD=$pc
		fi

		if [[ $p =~ 'ADD PRIMARY KEY'.* && $DPK ]];
		then
			DPKCMD=''
			DPK=false
		fi

		if [[ $p =~ 'DROP INDEX'.* ]];
		then
			DROPINDEX=`expr "$p" : '.*DROP INDEX \([a-zA-Z_]*\)'`
			pc=`echo $p | sed 's\,$\;\i'`
			#echo "$ALTERTABLE $pc"
			echo "$ALTERTABLE $pc" >> $ID
			echo $DROPINDEX >> $I
		fi

		if [[ $p =~ 'ADD UNIQUE INDEX'.* ]];
		then
			ADDINDEX=`expr "$p" : '.*ADD UNIQUE INDEX \([a-zA-Z_]*\)'`
			# echo "---$ADDINDEX"
			if [[ `cat $I | grep $ADDINDEX | wc -l` -eq 1 ]];
			then
				#echo "this index was droped: $ADDINDEX"
				sed -i 's\'$ADDINDEX'\needtoignore\i' $ID
			else
				pc=`echo $p | sed 's\,$\;\i'`
				#echo "1 $ALTERTABLE $pc"
				echo "$ALTERTABLE $pc" >> $ALTERDATE
			fi
		fi

		if [[ $p =~ 'ADD INDEX'.* ]];
		then
			ADDINDEX=`expr "$p" : '.*ADD INDEX \([a-zA-Z_]*\)'`
			FIELD=`expr "$p" : '.* (\([a-zA-Z_]*\))'`
			#echo "9 $FIELD"
			if [[ `cat $I | grep "$ADDINDEX" | wc -l` -eq 1 ]];
			then
				#echo "this index was droped: $ADDINDEX"
				sed -i 's\'$ADDINDEX'\needtoignore\i' $ID
			else
				pc=`echo $p | sed 's\,$\;\i'`
				#echo "2 $ALTERTABLE $pc"
				echo "$ALTERTABLE $pc" >> $ALTERDATE
			fi
		fi

		if [[ $p =~ 'CHANGE COLUMN'.* ]];
		then
			pc=`echo $p | sed 's\,$\;\i'`
			#echo "3 $ALTERTABLE $pc"
			echo "$ALTERTABLE $pc" >> $ALTERDATE
		fi

		if [[ $p =~ 'ADD COLUMN'.* ]];
		then
			pc=`echo $p | sed 's\,$\;\i'`
			echo "$ALTERTABLE $pc" >> $ALTERDATE
		fi
		if [[ $p =~ 'DROP COLUMN'.* ]];
		then
			pc=`echo $p | sed 's\,$\;\i'`
			echo "$ALTERTABLE $pc" >> $ALTERDATE
		fi

		if [[ $p =~ 'AUTO_INCREMENT'.* ]];
		then
			pc=`echo $p | sed 's\,$\;\i'`
			echo "$ALTERTABLE $pc" >> $ALTERDATE
		fi

		if [[ $p =~ 'ALTER TABLE'.* ]];
		then
			echo $p | sed 's\`'$DBTMP'`.\\i' > $T
			pp=`cat $T`
			ALTERTABLE=$pp
		fi

		if [[ $p =~ ';' ]];
		then
			cat $ID | grep -v needtoignore >> $ALTERDATE
			if [[ $DPKCMD -ne '' ]];
			then
				echo "5 $ALTERTABLE $DPKCMD"
				echo "$ALTERTABLE $DPKCMD" >> $ALTERDATE
			fi

			#echo 'end'
			echo '' > $T
			echo '' > $I
			echo '' > $ID
			DPK=false
			ALTERTABLE=''
		fi
	done < $FILETMP
	
	# echo "res----"
	# cat $ALTERDATE

	#remove new lines
 	sed -i ':a;N;$!ba;s/\n/ /g' $FILETMP
	sed -i 's/;/;\n /g' $FILETMP
	#sed -i 's/; /;/g' $FILETMP
	sed -i 's/;/;\n/g' $FILETMP


	FILETMPSIZE=$(stat -c%s "$FILETMP")
	if [ $FILETMPSIZE -gt 5 ]
	then
		cat $FILETMP | grep -v "^$" | grep -v "DATABASE" | grep -v "ALTER TABLE">> $ALTERDATE
		echo "ALTER ADDED: $ALTERNAME"

		DBALTERED=`cat $FILETMP | grep -v "^$" | grep "DATABASE" | wc -l`
		if [ $DBALTERED -gt 0 ];
			then
				cat $FILETMP | grep -v "^$" | grep "DATABASE" >> $ALTERDIR$DATABASENAME
				echo "DATABASENAME ADDED: $DATABASENAME"
			fi
	fi

	if [[ -f $ALTERDIR$CREATENAME ]]
	then
		echo $CREATENAME >> $EXECUTED
		mysql -h $LOCALIP -u $USER -p$PASSWORD $DBTMP < $ALTERDIR$CREATENAME
	fi

	if [[ -f $ALTERDIR$DROPNAME ]]
	then
		echo $DROPNAME >> $EXECUTED
		mysql -h $LOCALIP -u $USER -p$PASSWORD $DBTMP < $ALTERDIR$DROPNAME
	fi

	if [[ -f $ALTERDATE ]]
	then
		echo $ALTERNAME >> $EXECUTED
		mysql -h $LOCALIP -u $USER -p$PASSWORD $DBTMP < $ALTERDATE
	fi

	mysqldump  -d -h $LOCALIP -u $USER -p$PASSWORD $DB --compact | sed 's/ AUTO_INCREMENT=[0-9]*\b//' > $FILETMP

else
	echo "WRONG CONFIGURATION"
	exit 0
fi

#mysql notification enable
mysql -h $LOCALIP -u $USER -p$PASSWORD -e "SET sql_notes = 1;"

FILESIZE=$(stat -c%s "$FILE")
FILETMPSIZE=$(stat -c%s "$FILETMP")

if [ $FILESIZE -eq $FILETMPSIZE ]; then
	rm -f $FILETMP
else
	echo "DB UPDATED"
	mv $FILE $FILEDATE
	mv $FILETMP $FILE
	exit 0
fi

