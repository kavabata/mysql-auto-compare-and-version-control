#!/bin/bas

SQLDUMP='dump.txt'
A='afters.txt'
C='columns.txt'
T='tmp.txt'
R='results.txt'
DBTMP='pay4food_tmp'
DPK=false
S=false

FIRSTALTER=''
SECONDALTER=''

echo '' > A
echo '' > C
echo '' > T
echo '' > R

while read p; 
do
	if [[ $p =~ 'DROP PRIMARY KEY'.* ]];
	then
		DPK=true
		DPKCMD=$p
		# echo $p
	fi

	if [[ $p =~ 'ADD PRIMARY KEY'.* && $DPK ]];
	then
		DPKCMD=''
	fi

	if [[ $p =~ 'CHANGE COLUMN'.* ]];
	then
		#echo $p
		# AFTER=`expr "$p" : '.* AFTER \([a-zA-Z_]*\)'`
		# COLUMN=`expr "$p" : '.*CHANGE COLUMN \([a-zA-Z_]*\)'`
		# INC=`cat A | grep "$COLUMN" | wc -l`
		# INA=`cat C | grep "$AFTER" | wc -l`
		# cat A | grep "$COLUMN" | wc -l

		# if [[ $INC -eq 1 ]];
		# then
		# 	#exclude
		# 	SECONDALTER+=" $p \n"
		# 	S=true
		# else
		# 	#include
		# 	FIRSTALTER+=" $p \n"
		# fi

		pc=`echo $p | sed 's\,\;\i'`
		echo "$ALTERTABLE $pc" >> R
	fi

	if [[ $p =~ 'ALTER TABLE'.* ]];
	then
		echo $p | sed 's\`'$DBTMP'`.\\i' > $T
		pp=`cat $T`
		ALTERTABLE=$pp
	fi

	if [[ $p =~ ';' ]];
	then
		#FIRSTALTER+=$DPKCMD
		#echo -e $FIRSTALTER
		# if $S ;
		# then
		# 	echo "SECOND ALTER"
		# 	echo -e $SECONDALTER
		# 	S=false
		# fi

		# FIRSTALTER=SECONDALTER='';
		# echo '' > A
		# echo '' > C
		echo '' > T
		echo '
'
	fi

done < $SQLDUMP

# cat $SQLDUMP